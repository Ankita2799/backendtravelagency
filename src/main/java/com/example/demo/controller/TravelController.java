package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.TravelModel;
import com.example.demo.repository.TravelRepository;
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TravelController {
	@Autowired
	TravelRepository trepo;
	
	@PostMapping("/login")
	public TravelModel getUser(@RequestBody TravelModel model)
	{
		 return trepo.findByUsernameAndPassword(model.getUsername(),model.getPassword());
	}
	@PostMapping("/register")
	public String registerUser(@RequestBody TravelModel model)
	{
		trepo.save(model);
		return "User Registered Succesfully";
	}
}
