package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.TravelModel;

public interface TravelRepository extends JpaRepository<TravelModel, String>{

	TravelModel findByUsernameAndPassword(String username, String password);

}
